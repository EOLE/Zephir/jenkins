def getRefLastTag(String ref) {
    return sh(
        script: "git tag -l --points-at ${ref} --sort -taggerdate", 
        returnStdout: true
    ).split('\n')[0]
}