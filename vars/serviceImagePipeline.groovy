// Pipeline de construction des images Docker des services Zéphir
def call(String serviceName) {
    pipeline {

        agent {
            dockerfile {
                filename 'Dockerfile'
                dir 'ci'
                args '--privileged -v /var/run/docker.sock:/var/run/docker.sock'
		        additionalBuildArgs "--build-arg http_proxy=${env.HTTP_PROXY} --build-arg https_proxy=${env.HTTPS_PROXY}"
            }
        }

        triggers {
            gitlab(
                triggerOnPush: true, 
                triggerOnMergeRequest: true, 
                branchFilterType: 'All',
                cancelPendingBuildsOnUpdate: false
            )
        }

        options {
            gitLabConnection('Gitlab MIM')
            gitlabBuilds(builds: [])
        }

        parameters {
            string(defaultValue: "develop", description: 'Branche "staging"', name: 'stagingBranch')
            string(defaultValue: "master", description: 'Branche "stable"', name: 'stableBranch')
        }

        environment {
            IMAGE_NAME = "${serviceName}"
            IMAGE_COMMIT_TAG =  env.GIT_COMMIT.substring(0,8)
        }

        stages {

            // Construction de l'image Docker
            stage('Build image') {
                steps {
                    script {
                        hook("pre-image-build")
                        buildServiceImageStage()
                        hook("post-image-build")
                    }
                }
            }

            // Publication de l'image en dev/staging
            // en fonction de la présence d'un tag 
            // sur le commit et de la branche courante.
            stage('Publish dev/staging image') {
                when {
                    expression {
                        return env.GIT_BRANCH == "origin/${params.stagingBranch}"
                    }
                }
                steps {
                    script {
                        hook("pre-staging-image-publish")
                        publishDevStagingImageStage()
                        hook("post-staging-image-publish")
                    }
                }
            }

            // Publication de l'image en stable
            // en fonction de la branche courante.
            // La présence d'un tag est obligatoire.
            stage('Publish stable image') {
                when {
                    expression {
                        return env.GIT_BRANCH == "origin/${params.stableBranch}"
                    }
                }
                steps {
                    script {
                        hook("pre-stable-image-publish")
                        publishStableImageStage()
                        hook("post-stable-image-publish")
                    }
                }
            }
        }
    }
}

def buildServiceImageStage() {
    gitlab.stage(STAGE_NAME) {
        zephirDocker.withHubCredentials {
            sh """
                # Construction de l'image
                docker build --no-cache --build-arg http_proxy='${env.HTTP_PROXY}' --build-arg https_proxy='${env.HTTPS_PROXY}' -t '${DOCKER_USERNAME}/${IMAGE_NAME}' .

                # Ajout du tag de commit
                docker tag '${DOCKER_USERNAME}/${IMAGE_NAME}:latest' '${DOCKER_USERNAME}/${IMAGE_NAME}:${IMAGE_COMMIT_TAG}'
            """
        }
    }
}

def publishDevStagingImageStage() {
    gitlab.stage(STAGE_NAME) {
        // On récupère le dernier tag publié sur le commit courant
        def lastTag = git.getRefLastTag(env.GIT_COMMIT)
        if (!lastTag) {
            // Si aucun tag n'est associé au commit, alors on 
            // publie l'image comme une image de développement
            zephirDocker.addTagAndPublishImage(
                env.IMAGE_COMMIT_TAG, 
                "dev-latest"
            )
        } else {
            // Si un tag est associé au commit, alors on publie l'image
            // comme une image de staging
            zephirDocker.addTagAndPublishImage(
                env.IMAGE_COMMIT_TAG, 
                "staging-${lastTag}"
            )
            zephirDocker.addTagAndPublishImage(
                env.IMAGE_COMMIT_TAG, 
                "staging-latest"
            )
        }
    }
}

def publishStableImageStage() {
    gitlab.stage(STAGE_NAME) {
        def lastTag = git.getRefLastTag(env.GIT_COMMIT)
        if (!lastTag) {
            error("""
            Aucun tag n'est associé avec la référence '${env.GIT_COMMIT}'. 
            La publication de l'image Docker 'stable' requiert un numéro de version.
            """.stripIndent())
        }
        zephirDocker.addTagAndPublishImage(
            env.IMAGE_COMMIT_TAG, 
            "stable-${lastTag}"
        )
        zephirDocker.addTagAndPublishImage(
            env.IMAGE_COMMIT_TAG, 
            "stable-latest"
        )
    }
}
