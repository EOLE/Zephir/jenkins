def call(String name) {
    def rootDir = pwd()
    def filepath = "${rootDir}/.jenkins/${name}.groovy"
    def exists = fileExists(filepath)
    if (!exists) {
        println("No hook '${filepath}' script. Skipping.")
        return
    }
    def hook = load(filepath)
    if(hook.metaClass.respondsTo(hook, 'exec')) {
        hook.exec()
    } else {
        error("Hook script '${filepath}' exists but does not expose an exec() function.")
    }
}