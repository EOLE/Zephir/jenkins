def stage(String stageName, Closure fn) {
    gitlabBuilds(builds: [stageName]) {
        gitlabCommitStatus(stageName) {
            fn()
        }
    }
}
