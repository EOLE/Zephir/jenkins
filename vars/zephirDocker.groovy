def withHubCredentials(Closure fn) {
    withCredentials([
        usernamePassword(credentialsId: 'dockerhub', usernameVariable: 'DOCKER_USERNAME', passwordVariable: 'DOCKER_PASSWORD')]
    ){
        fn()
    }
}

def addTagAndPublishImage(String originalTag, String newTag) {
    withHubCredentials {
        sh """
            # Ajout du nouveau tag
            docker tag '${DOCKER_USERNAME}/${IMAGE_NAME}:${originalTag}' '${DOCKER_USERNAME}/${IMAGE_NAME}:${newTag}'

            # Publication de l'image sur le DockerHub
            echo "${DOCKER_PASSWORD}" | docker login --username '${DOCKER_USERNAME}' --password-stdin
            docker push '${DOCKER_USERNAME}/${IMAGE_NAME}:${newTag}'
            docker push '${DOCKER_USERNAME}/${IMAGE_NAME}:${originalTag}'
            docker logout
        """
    }
}